# HHKB实践无线蓝牙连接



#### 收到板子

![硬件](meta/WechatIMG65.png)



#### 踩坑

不要用Mac来刷系统，我试了CoolTerm, comtoll, SerialPortUtility,SecureCrt, 都没成功让板子出现ccccc, 最终在虚拟机里，成功烧录。



#### 硬件连接

按照教程，先连接硬件。不知道为啥，连接键盘的这个板子，一会灯亮，一会灯不亮。我也不知道是我刷的固件问题还是啥，反正感觉不太正常。

![硬件连接](meta/WechatIMG64.png)



#### 刷固件

安装好教程中所说的ch40驱动，和securecrt。

然后打开SecureCRT

![SecureCRT](meta/WechatIMG66.png)

点击连接后， 会收到LED_SPARK! 这几个字。

![SecureCRT](meta/WechatIMG67.png)



在SecureCRT这里激活下当前窗口， 按住电脑 ESC,  然后再按板子上的rst按钮， 进入烧录模式。

![SecureCRT](meta/WechatIMG68.png)



等待烧录完成。

![SecureCRT](meta/WechatIMG69.png)



烧录完成后，按rst， 重启了板子。可以通过蓝牙搜索到Harmony的板子，并连接。

![SecureCRT](meta/WechatIMG70.png)



很遗憾，在记事本里打字，无任何效果。 不知道是不是需要自己写驱动？还是啥？ 现在只吃吃亮下Num灯？

待明天客服解决哈~